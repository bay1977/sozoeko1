#' Cobb-Douglas production cross-section data for 33 firms
#'
#' The Cobb-Douglas production function is particularly suitable for modeling larger economic systems (from larger firms to entire economies). In this application, it models the combination of the input factors capital and labor to produce output. The Cobb-Douglas function can be easily represented as an econometric model by some transformations. Taken from book: Hill, Griffiths and Lim: Principles of Econometrics (4e)
#'
#' @format A tibble with 33 rows and 3 variables:
#' \describe{
#'   \item{k}{dbl Capital input}
#'   \item{l}{dbl Labor input}
#'   \item{q}{dbl Output}
#' }
#' @source Hill, Griffiths and Lim: ``Principles of Econometrics (4e)``
#'
#' \url{http://sourceforge.net/projects/gretl/files/datafiles/POE4data.tar.gz/download}
"cobb_douglas" # Name Ihres Datensatzes
