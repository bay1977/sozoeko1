#' sozoeko1: Ein Paket, welches die Lehre der Veranstaltung Oekonometrie 1 im Bachelor Sozialoekonomie an der WiSo-Fakultaet der Uni Hamburg unterstuetzt.
#'
#' Das \pkg{sozoeko1} Paket besteht aus sechs Datensaetzen, welche im Rahmen der Lehre verwendet werden:
#' cobb-douglas, eduinc, food, gfc, phillips und qtm. Die Daten stammen ueberwiegend aus dem Lehrbuch Hill, Griffiths and Lim: ``Principles of Econometrics (4e)``. Teilweise haben eigene Anpassungen stattgefunden. Diese sind in den Hilfe-Seiten zu den einzelnen Datensaetzen dokumentiert.
#'
#' @section cobb-douglas Datensatz:
#' Cobb-Douglas Produktions-Querschnittsdaten fuer 33 Firmen \cr
#' \strong{Benutzung:} ``cobb_douglas``
#'
#' @section eduinc Datensatz:
#' Querschnittsdaten zu formaler Ausbildungsdauer und Einkommen von Familien mit berufstaetigen Frauen \cr
#' \strong{Benutzung:} ``eduinc``
#'
#' @section food Datensatz:
#' Querschnittsdaten, welche Einkommen und Ausgaben fuer Nahrungsmittel beinhalten \cr
#' \strong{Benutzung:} ``food``
#'
#' @section gfc Datensatz:
#' Zeitreihen-Daten fuer das BIP der USA und der Eurozone \cr
#' \strong{Benutzung:} ``gfc``
#'
#' @section phillips Datensatz:
#' Zeitreihendaten fuer die australische Phillips-Kurve \cr
#' \strong{Benutzung:} ``phillips``
#'
#' @section qtm Datensatz:
#' Zeitreihendaten fuer Geldmenge und Preisniveau \cr
#' \strong{Benutzung:} ``qtm``
#'
#' @docType package
#' @name sozoeko1
NULL
